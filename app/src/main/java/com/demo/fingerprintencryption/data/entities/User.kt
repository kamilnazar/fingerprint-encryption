package com.demo.fingerprintencryption.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class User(@ColumnInfo(name = "username") val username: String,
                @ColumnInfo(name = "password") val password: String,
                @ColumnInfo(name = "token") val token: String) {
    @ColumnInfo(name = "id")
    @PrimaryKey
    var id = 1L
}