package com.demo.fingerprintencryption.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.demo.fingerprintencryption.data.entities.EncryptedFileTable

@Dao
interface EncryptedFileDao {
    @Query("select * from encrypted_file")
    fun getAllFiles(): LiveData<List<EncryptedFileTable>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(file: EncryptedFileTable): Long

    @Update
    fun update(file: EncryptedFileTable)

    @Query("SELECT * FROM encrypted_file where id=:id LIMIT 1")
    fun getFileById(id: Long): EncryptedFileTable

    @Query("SELECT * FROM encrypted_file where id=:id LIMIT 1")
    fun getFileByIdLiveData(id: Long): LiveData<EncryptedFileTable>
}