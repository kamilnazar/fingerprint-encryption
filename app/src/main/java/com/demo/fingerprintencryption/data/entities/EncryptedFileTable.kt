package com.demo.fingerprintencryption.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "encrypted_file")
data class EncryptedFileTable(@ColumnInfo(name = "name") var name: String,
                              @ColumnInfo(name = "url") var downloadUrl: String? = null,
                              @ColumnInfo(name = "completed") var completed: Boolean = false,
                              @ColumnInfo(name = "local_file_path") var encryptedFilePath: String? = null,
                              @ColumnInfo(name = "decrypted_file_path") var decryptedFilePath: String? = null,
                              @ColumnInfo(name = "org_file_path") var orgFilePath: String? = null

) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0

    val filename: String get() = this.name
}