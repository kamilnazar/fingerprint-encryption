package com.demo.fingerprintencryption.data.repository

import androidx.annotation.WorkerThread
import com.demo.fingerprintencryption.data.dao.EncryptedFileDao
import com.demo.fingerprintencryption.data.entities.EncryptedFileTable

class EncryptedFileRepository(private val encryptedFileDao: EncryptedFileDao) {
    val allFiles = encryptedFileDao.getAllFiles()

    @WorkerThread
    fun insert(encryptedFile: EncryptedFileTable): Long {
        return encryptedFileDao.insert(encryptedFile)
    }

    @WorkerThread
    fun update(encryptedFile: EncryptedFileTable) {
         encryptedFileDao.update(encryptedFile)
    }

    fun getById(id: Long) = encryptedFileDao.getFileById(id)
    fun getByIdLiveData(id: Long) = encryptedFileDao.getFileByIdLiveData(id)
}