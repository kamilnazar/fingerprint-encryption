package com.demo.fingerprintencryption.data.dao

import android.database.sqlite.SQLiteDatabase
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.demo.fingerprintencryption.data.entities.User

@Dao
interface UserDao {
    @Query("select * from users")
    fun getUser(): LiveData<List<User>>

    @Insert(onConflict = SQLiteDatabase.CONFLICT_REPLACE)
    fun insert(user: User)

    @Delete
    fun delete(user: User)
}