package com.demo.fingerprintencryption.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.demo.fingerprintencryption.data.dao.EncryptedFileDao
import com.demo.fingerprintencryption.data.dao.UserDao
import com.demo.fingerprintencryption.data.entities.EncryptedFileTable
import com.demo.fingerprintencryption.data.entities.User

@Database(entities = [EncryptedFileTable::class, User::class], version = 1)
abstract class EncryptedFileDatabase : RoomDatabase() {
    abstract fun encryptedFileDao(): EncryptedFileDao
    abstract fun userDao(): UserDao
}