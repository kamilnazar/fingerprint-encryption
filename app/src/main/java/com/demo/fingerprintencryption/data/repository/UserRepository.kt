package com.demo.fingerprintencryption.data.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.demo.fingerprintencryption.data.dao.UserDao
import com.demo.fingerprintencryption.data.entities.User

class UserRepository(private val userDao: UserDao) {
    @WorkerThread
    fun insert(user: User) {
        userDao.insert(user)
    }

    @WorkerThread
    fun delete(user: User) {
        userDao.delete(user)
    }

    fun getUser(): LiveData<List<User>> {
        return userDao.getUser()
    }
}