package com.demo.fingerprintencryption.login

import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides

@Module
class LoginModule(private val loginFragment: LoginFragment) {
    @Provides
    fun provideNavigation(): LoginNavigation = loginFragment

    @Provides
    fun provideViewModel(viewModelFactory: LoginViewModelFactory) = ViewModelProviders.of(loginFragment, viewModelFactory)
            .get(LoginViewModel::class.java)
}