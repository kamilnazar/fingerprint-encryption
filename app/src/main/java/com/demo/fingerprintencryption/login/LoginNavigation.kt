package com.demo.fingerprintencryption.login

interface LoginNavigation {
    fun gotoFileListActivity()
    fun showFingerPrintAuthDialog()
}