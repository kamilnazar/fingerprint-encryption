package com.demo.fingerprintencryption.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import co.infinum.goldfinger.Error
import co.infinum.goldfinger.Goldfinger
import com.demo.fingerprintencryption.R
import com.demo.fingerprintencryption.base.BaseFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.finger_print_bottomsheet.*
import timber.log.Timber
import javax.inject.Inject

class LoginFragment : BaseFragment(), LoginNavigation {
    @Inject
    lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        login_button.setOnClickListener {
            loginViewModel.login(login_email_input.text.toString(), login_password_input.text.toString())
        }


        val finger = Goldfinger.Builder(context).build()

        if (!finger.hasEnrolledFingerprint()) {
            AlertDialog.Builder(context!!).setCancelable(false)
                    .setMessage("Set fingerprint")
                    .setNeutralButton("OK") { dialog, which ->
                        activity?.finish()
                    }.create().show()
        } else {
            subscribeToViewModel()
            login_use_finger_print_button.setOnClickListener {
                showFingerPrintAuthDialog()
            }
        }
    }

    override fun showFingerPrintAuthDialog() {
        val dialog = FingerPrintDialog()
        dialog.callback = object : FingerPrintDialogCallback {
            override fun success() {
                gotoFileListActivity()
            }

            override fun failed() {
            }

        }
        dialog.show(fragmentManager!!, dialog.tag)
    }

    private fun subscribeToViewModel() {
        loginViewModel.loginProgress.observe(this, Observer {
            if (it)
                showProgress()
            else hideProgress()
        })

        loginViewModel.loginError.observe(this, Observer {
            if (it)
                Snackbar.make(login_root, getString(R.string.login_failed_message), Snackbar.LENGTH_SHORT).show()
        })
        loginViewModel.users.observe(this, Observer {
            val finger = Goldfinger.Builder(context).build()
            if (finger.hasEnrolledFingerprint() && it.isNotEmpty())
                login_use_finger_print_button.visibility = View.VISIBLE
            if (it.isNotEmpty())
                showFingerPrintAuthDialog()
        })
    }

    override fun gotoFileListActivity() {
        this.findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToFileListFragment())
    }

    private fun showProgress() {
        login_button.visibility = View.GONE
        login_progress.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        login_button.visibility = View.VISIBLE
        login_progress.visibility = View.GONE
    }

    override fun inject() {
        appComponent.subComponent(LoginModule(this))
                .inject(this)
    }

    companion object {
        const val USERNAME = "com.demo.fingerprintecryption.USERNAME"
        const val PASSWORD = "com.demo.fingerprintecryption.PASSWORD"
    }
}

class FingerPrintDialog : BottomSheetDialogFragment() {

    var callback: FingerPrintDialogCallback? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.finger_print_bottomsheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val finger = Goldfinger.Builder(context).build()
        finger.authenticate(object : Goldfinger.Callback() {
            override fun onSuccess(value: String?) {
                fingerfrint_status.setImageResource(R.drawable.ic_done_green_24dp)
                dismiss()
                callback?.success()
            }

            override fun onError(error: Error?) {
                Timber.i(error.toString())
                fingerfrint_status.setImageResource(R.drawable.ic_close_red_24dp)
                callback?.failed()
            }
        })
    }
}

interface FingerPrintDialogCallback {
    fun success()
    fun failed()
}