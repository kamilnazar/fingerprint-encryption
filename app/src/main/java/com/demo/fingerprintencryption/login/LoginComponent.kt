package com.demo.fingerprintencryption.login

import dagger.Subcomponent

@LoginScope
@Subcomponent(modules = [LoginModule::class])
interface LoginComponent {
    fun viewModel(): LoginViewModel
    fun inject(fragment: LoginFragment)
}