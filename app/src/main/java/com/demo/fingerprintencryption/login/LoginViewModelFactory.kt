package com.demo.fingerprintencryption.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.demo.fingerprintencryption.api.Api
import com.demo.fingerprintencryption.data.repository.UserRepository
import javax.inject.Inject

@LoginScope
class LoginViewModelFactory @Inject constructor(private val loginNavigation: LoginNavigation, private val userRepository: UserRepository, private val api: Api) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(loginNavigation, userRepository, api) as T
    }
}