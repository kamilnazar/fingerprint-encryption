package com.demo.fingerprintencryption.login

import androidx.lifecycle.MutableLiveData
import com.demo.fingerprintencryption.api.Api
import com.demo.fingerprintencryption.api.requestparams.LoginParams
import com.demo.fingerprintencryption.base.BaseViewModel
import com.demo.fingerprintencryption.data.entities.User
import com.demo.fingerprintencryption.data.repository.UserRepository
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import timber.log.Timber

class LoginViewModel(private val loginNavigation: LoginNavigation,
                     private val userRepository: UserRepository,
                     private val api: Api) : BaseViewModel() {
    private val _loginProgress: MutableLiveData<Boolean> = MutableLiveData()
    val loginProgress get() = _loginProgress

    private val _loginError: MutableLiveData<Boolean> = MutableLiveData()
    val loginError get() = _loginError

    val users = userRepository.getUser()

    fun login(userName: String, password: String) {
        uiScope.launch {
            _loginProgress.value = (true)
            try {
                withContext(IO) {
                    val token = api.login(LoginParams(userName, password)).await()
                    userRepository.insert(User(userName, password, token.token))
                }
                loginNavigation.gotoFileListActivity()
            } catch (e: HttpException) {
                Timber.e(e)
                _loginError.value = (true)
            } finally {
                _loginProgress.value = (false)
            }
        }
    }
}