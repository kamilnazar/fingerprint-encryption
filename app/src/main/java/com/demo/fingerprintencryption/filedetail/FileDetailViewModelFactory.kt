package com.demo.fingerprintencryption.filedetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.demo.fingerprintencryption.data.repository.EncryptedFileRepository
import com.demo.fingerprintencryption.filelist.FileListScope
import com.demo.fingerprintencryption.filelist.FileListViewModel
import javax.inject.Inject

@FileDetailScope
class FileDetailViewModelFactory @Inject constructor(val repository: EncryptedFileRepository,
                                                     val fileId: Long) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FileDetailViewModel(repository, fileId) as T
    }
}