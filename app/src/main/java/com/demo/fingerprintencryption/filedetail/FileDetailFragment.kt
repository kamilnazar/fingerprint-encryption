package com.demo.fingerprintencryption.filedetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.demo.fingerprintencryption.base.BaseFragment
import kotlinx.android.synthetic.main.activity_file_detail.*
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import android.widget.Toast
import android.content.ActivityNotFoundException
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import android.webkit.MimeTypeMap
import android.net.Uri
import androidx.core.content.FileProvider
import com.demo.fingerprintencryption.R
import com.demo.fingerprintencryption.data.entities.EncryptedFileTable
import java.io.File


class FileDetailFragment : BaseFragment() {
    @Inject
    lateinit var viewModel: FileDetailViewModel
    lateinit var encryptedFileTable: EncryptedFileTable

    override fun inject() {
        appComponent.subComponent(FileDetailModule(this, args.fileId)).inject(this)
    }

    val args: FileDetailFragmentArgs by navArgs()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_file_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.encryptedFile.observe(this, androidx.lifecycle.Observer { t ->
            file_name_txt.text = t.filename
            encryptedFileTable = t
        })
        decryptBtn.setOnClickListener { openFile(File(encryptedFileTable.orgFilePath)) }
    }

    private fun openFile(fileDecrypted: File) {
        val myMime = MimeTypeMap.getSingleton()
        val newIntent = Intent(Intent.ACTION_VIEW)
        val mimeType = myMime.getMimeTypeFromExtension(fileDecrypted.extension)
        val uri =FileProvider.getUriForFile(context!!, context!!.applicationContext.packageName + ".fileprovider", fileDecrypted)
        newIntent.setDataAndType(uri, mimeType)
        newIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        newIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION;
        try {
            context?.startActivity(newIntent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(context, "No handler for this type of file.", Toast.LENGTH_LONG).show()
        }

    }
}