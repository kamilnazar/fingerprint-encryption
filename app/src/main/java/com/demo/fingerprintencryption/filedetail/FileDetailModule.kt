package com.demo.fingerprintencryption.filedetail

import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides

@Module
class FileDetailModule(val fragment: FileDetailFragment,val fileId:Long) {
    @Provides
    fun provideViewModel(factory: FileDetailViewModelFactory) = ViewModelProviders.of(fragment,factory).get(FileDetailViewModel::class.java)
    @Provides fun provideFileId()=fileId
}