package com.demo.fingerprintencryption.filedetail

import dagger.Subcomponent

@FileDetailScope
@Subcomponent(modules = [FileDetailModule::class])
interface FileDetailComponent {
    fun getViewModel():FileDetailViewModel
    fun inject(fragment: FileDetailFragment)
}