package com.demo.fingerprintencryption.filedetail

import android.os.Environment
import androidx.lifecycle.MutableLiveData
import com.demo.fingerprintencryption.base.BaseViewModel
import com.demo.fingerprintencryption.data.repository.EncryptedFileRepository
import com.demo.fingerprintencryption.workers.FileEncryptWorker
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.io.File
import javax.crypto.Cipher
import javax.crypto.CipherOutputStream
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

class FileDetailViewModel(private val repository: EncryptedFileRepository,
                          private val fileId: Long) : BaseViewModel() {
    val encryptedFile = repository.getByIdLiveData(fileId)

    private val _decryptionProgress = MutableLiveData<Boolean>()
    val decriptionProgress get() = _decryptionProgress

    fun decryptFile() {
        uiScope.launch(IO) {
            _decryptionProgress.postValue(true)
            val file = File(encryptedFile.value?.encryptedFilePath)

            val decryptedFile = File("${Environment.getExternalStorageDirectory()}/DecryptedFiles/${encryptedFile.value?.name}")
            val cipher = generateCipher()

            val decryptedFileOutStream = CipherOutputStream(decryptedFile.outputStream(), cipher)

            decryptedFileOutStream.use { outputStream ->
                file.inputStream().use { inputStream ->
                    val buffer = ByteArray(1024)
                    var block = inputStream.read(buffer)
                    while (block >= 0) {
                        outputStream.write(buffer, 0, block)
                        block = inputStream.read(buffer)
                    }
                }
            }

            cipher.doFinal()
            encryptedFile.value?.decryptedFilePath = decryptedFile.absolutePath
            repository.insert(encryptedFile.value!!)
            _decryptionProgress.postValue(false)
        }
    }

    private fun generateCipher(): Cipher {
        val secretKeyFactory = SecretKeyFactory.getInstance(FileEncryptWorker.KEY_ALGO)
        val keySpec = PBEKeySpec(FileEncryptWorker.ENCRYPTION_KEY.toCharArray(), FileEncryptWorker.ENCRYPTION_KEY_SALT.toByteArray(charset(FileEncryptWorker.CHARSET)), 100, 256)
//        val secretKey = SecretKeySpec(ENCRYPTION_KEY.toByteArray(charset(CHARSET)), ENCRYPTION_ALGO)
        val secretKey = secretKeyFactory.generateSecret(keySpec)
        val cipher: Cipher = Cipher.getInstance(FileEncryptWorker.ENCRYPTION_ALGO)
        cipher.init(Cipher.DECRYPT_MODE, secretKey)
        return cipher
    }
}