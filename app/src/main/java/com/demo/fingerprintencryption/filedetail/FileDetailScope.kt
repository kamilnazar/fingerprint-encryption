package com.demo.fingerprintencryption.filedetail

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FileDetailScope