package com.demo.fingerprintencryption.workers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.demo.fingerprintencryption.App
import com.demo.fingerprintencryption.api.Api
import com.demo.fingerprintencryption.data.repository.EncryptedFileRepository
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import timber.log.Timber
import java.io.File
import java.io.IOException
import javax.inject.Inject

class FileUploadWorker(val context: Context, private val workerParameters: WorkerParameters) : Worker(context, workerParameters) {

    @Inject
    lateinit var api: Api
    @Inject
    lateinit var repo: EncryptedFileRepository

    override fun doWork(): Result {
        (applicationContext as App).appComponent
                .inject(this)
        val encryptedFilePath = inputData.getString(FileEncryptWorker.ENCRYPTED_FILE_PATH)
        val encryptedFile = File(encryptedFilePath)

        val filePath = inputData.getString(FileCopyWorker.FILE_OUTPUT_PATH)

        val mediaType = MediaType.parse("multipart/form-data")
        try {
            val fileId = inputData.getLong(FILE_ROW_ID, 0)
            val encryptedFileTable = repo.getById(fileId)

            val response = api.uploadFile(
                    fileName = RequestBody.create(mediaType, encryptedFileTable.name),
                    extension = RequestBody.create(mediaType, encryptedFile.nameWithoutExtension.substringAfterLast(".")),
                    file = MultipartBody.Part.createFormData("url", encryptedFile.name, RequestBody.create(mediaType, encryptedFile))
            ).execute().body()!!

            encryptedFileTable.completed = true
            encryptedFileTable.downloadUrl = response.url
            encryptedFileTable.name = response.name
            encryptedFileTable.orgFilePath = filePath

            repo.update(encryptedFileTable)
        } catch (e: IOException) {
            Timber.e(e)
            return Result.retry()
        }

        return Result.success()
    }


    companion object {
        const val FILE_ROW_ID = "file_row_id"
    }
}