package com.demo.fingerprintencryption.workers

import android.content.Context
import android.net.Uri
import android.os.Environment
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.util.*

class FileCopyWorker(context: Context, workerParameters: WorkerParameters) : Worker(context, workerParameters) {
    override fun doWork(): Result {
        val uri = Uri.parse(inputData.getString(FILE_INPUT_URI))
        val inputfile = File(uri.path)
        val outputFile = File("${Environment.getExternalStorageDirectory()}/EncryptedFiles/${UUID.randomUUID()}-${inputfile.name}")
        try {
            outputFile.mkdirs()
            inputfile.copyTo(outputFile, true, 1024)
        } catch (e: IOException) {
            Timber.e(e)
            return Result.failure()
        }
        val outputData = workDataOf(
                FILE_OUTPUT_PATH to outputFile.absolutePath
        )
        return Result.success(outputData)
    }

    companion object {
        const val FILE_OUTPUT_PATH = "file_output_path"
        const val FILE_INPUT_URI = "file_input_uri"
    }
}