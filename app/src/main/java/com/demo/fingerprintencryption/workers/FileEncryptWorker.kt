package com.demo.fingerprintencryption.workers

import android.content.Context
import android.os.Environment
import androidx.work.Worker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import java.io.File
import javax.crypto.Cipher
import javax.crypto.CipherOutputStream
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

class FileEncryptWorker(context: Context, workerParameters: WorkerParameters) : Worker(context, workerParameters) {
    override fun doWork(): Result {
        val filePath = inputData.getString(FileCopyWorker.FILE_OUTPUT_PATH)
        val originalFile = File(filePath)
        val encryptedFile = File("${Environment.getExternalStorageDirectory()}/EncryptedFiles/${originalFile.name}.encrypted")

        val secretKeyFactory = SecretKeyFactory.getInstance(KEY_ALGO)
        val keySpec = PBEKeySpec(ENCRYPTION_KEY.toCharArray(), ENCRYPTION_KEY_SALT.toByteArray(charset(CHARSET)), 100, 256)
//        val secretKey = SecretKeySpec(ENCRYPTION_KEY.toByteArray(charset(CHARSET)), ENCRYPTION_ALGO)
        val secretKey = secretKeyFactory.generateSecret(keySpec)
        val cipher: Cipher = Cipher.getInstance(ENCRYPTION_ALGO)
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)

        val encryptedFileOutputStream = encryptedFile.outputStream()
        val cipherOutputStream = CipherOutputStream(encryptedFileOutputStream, cipher)

        cipherOutputStream.use { outputStream ->
            originalFile.inputStream().use { inputStream ->
                val buffer = ByteArray(1024)
                var block = inputStream.read(buffer)
                while (block >= 0) {
                    outputStream.write(buffer, 0, block)
                    block = inputStream.read(buffer)
                }
            }
        }
        cipher.doFinal()

//        originalFile.delete()
        encryptedFileOutputStream.close()

        val outputData = workDataOf(ENCRYPTED_FILE_PATH to encryptedFile.absolutePath,
                FileCopyWorker.FILE_OUTPUT_PATH to filePath)
        return Result.success(outputData)
    }


    companion object {
        const val ENCRYPTION_KEY = "asdfghjkl"
        const val ENCRYPTION_KEY_SALT = "asdfghjkl"
        const val CHARSET = "UTF-8"
        const val ENCRYPTION_ALGO = "AES"
        const val KEY_ALGO = "PBKDF2WithHmacSHA1"
        const val ENCRYPTED_FILE_PATH = "encrypted_file_path"
    }
}