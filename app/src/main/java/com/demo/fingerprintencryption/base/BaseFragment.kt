package com.demo.fingerprintencryption.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.demo.fingerprintencryption.App
import com.demo.fingerprintencryption.di.components.AppComponent

abstract class BaseFragment : Fragment() {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inject()
    }

    abstract fun inject()

    val appComponent: AppComponent
        get() = (activity!!.applicationContext as App).appComponent
}