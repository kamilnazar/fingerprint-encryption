package com.demo.fingerprintencryption.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*

abstract class BaseViewModel : ViewModel() {
    val uiScope = CoroutineScope(Dispatchers.Main + Job())

    @ExperimentalCoroutinesApi
    override fun onCleared() {
        super.onCleared()
        uiScope.cancel()
    }
}