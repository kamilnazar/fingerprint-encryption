package com.demo.fingerprintencryption.di.modules

import android.content.Context
import androidx.room.Room
import com.demo.fingerprintencryption.data.db.EncryptedFileDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {
    @Provides @Singleton
    fun provideDb(context: Context): EncryptedFileDatabase {
        return Room.databaseBuilder(
                context.applicationContext,
                EncryptedFileDatabase::class.java,
                "EncryptedFileDatabase"
        ).build()
    }
}