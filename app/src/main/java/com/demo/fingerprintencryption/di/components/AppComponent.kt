package com.demo.fingerprintencryption.di.components

import android.content.Context
import com.demo.fingerprintencryption.api.Api
import com.demo.fingerprintencryption.data.repository.EncryptedFileRepository
import com.demo.fingerprintencryption.di.modules.AppModule
import com.demo.fingerprintencryption.di.modules.DbModule
import com.demo.fingerprintencryption.di.modules.NetworkModule
import com.demo.fingerprintencryption.di.modules.RepositoryModule
import com.demo.fingerprintencryption.filedetail.FileDetailComponent
import com.demo.fingerprintencryption.filedetail.FileDetailModule
import com.demo.fingerprintencryption.filelist.FileListComponent
import com.demo.fingerprintencryption.filelist.FileListModule
import com.demo.fingerprintencryption.login.LoginComponent
import com.demo.fingerprintencryption.login.LoginModule
import com.demo.fingerprintencryption.workers.FileUploadWorker
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, DbModule::class, RepositoryModule::class, NetworkModule::class])
interface AppComponent {
    fun context(): Context
    fun encryptedFileRepo(): EncryptedFileRepository
    fun api(): Api

    fun subComponent(module: FileListModule): FileListComponent
    fun subComponent(module: LoginModule): LoginComponent
    fun subComponent(module: FileDetailModule): FileDetailComponent
    fun inject(app: FileUploadWorker)
}