package com.demo.fingerprintencryption.di.modules

import com.demo.fingerprintencryption.data.db.EncryptedFileDatabase
import com.demo.fingerprintencryption.data.repository.EncryptedFileRepository
import com.demo.fingerprintencryption.data.repository.UserRepository
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {
    @Provides
    fun provideEncryptedFileRepo(database: EncryptedFileDatabase): EncryptedFileRepository {
        return EncryptedFileRepository(database.encryptedFileDao())
    }

    @Provides
    fun provideuserRepo(database: EncryptedFileDatabase) = UserRepository(database.userDao())
}