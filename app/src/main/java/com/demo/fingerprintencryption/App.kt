package com.demo.fingerprintencryption

import android.app.Application
import com.demo.fingerprintencryption.di.components.AppComponent
import com.demo.fingerprintencryption.di.components.DaggerAppComponent
import com.demo.fingerprintencryption.di.modules.AppModule
import com.demo.fingerprintencryption.di.modules.NetworkModule
import com.facebook.stetho.Stetho
import timber.log.Timber

class App : Application() {
    lateinit var appComponent: AppComponent
    override fun onCreate() {
        super.onCreate()
        createAppComponent()
        createDebugConfig()
    }

    private fun createDebugConfig() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this)
        }
    }

    private fun createAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .networkModule(NetworkModule(BASE_URL))
                .build()
    }
    companion object {
        const val BASE_URL="http://akash1996.pythonanywhere.com"
    }
}