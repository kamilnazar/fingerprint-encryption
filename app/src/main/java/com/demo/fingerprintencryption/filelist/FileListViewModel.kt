package com.demo.fingerprintencryption.filelist

import android.net.Uri
import androidx.work.*
import com.demo.fingerprintencryption.base.BaseViewModel
import com.demo.fingerprintencryption.data.entities.EncryptedFileTable
import com.demo.fingerprintencryption.data.repository.EncryptedFileRepository
import com.demo.fingerprintencryption.workers.FileCopyWorker
import com.demo.fingerprintencryption.workers.FileEncryptWorker
import com.demo.fingerprintencryption.workers.FileUploadWorker
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File

class FileListViewModel(private val repository: EncryptedFileRepository) : BaseViewModel() {

    val fileList = repository.allFiles

    fun scheduleWork(uri: Uri) = uiScope.launch(Dispatchers.IO) {
        val encryptedFileTable = EncryptedFileTable(name = File(uri.path).name).apply {
            orgFilePath = File(uri.path).absolutePath
        }
        encryptedFileTable.id = repository.insert(encryptedFileTable)
        val copyWork = OneTimeWorkRequestBuilder<FileCopyWorker>()
                .setInputData(workDataOf(FileCopyWorker.FILE_INPUT_URI to uri.toString()))
                .build()
        val encryptWork = OneTimeWorkRequestBuilder<FileEncryptWorker>()
                .build()
        val uploadWorker = OneTimeWorkRequestBuilder<FileUploadWorker>()
                .setConstraints(Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED)
                        .build())
                .setInputData(workDataOf(FileUploadWorker.FILE_ROW_ID to encryptedFileTable.id))
                .build()
        WorkManager.getInstance().beginWith(copyWork)
                .then(encryptWork)
                .then(uploadWorker)
                .enqueue()
    }
}