package com.demo.fingerprintencryption.filelist

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FileListScope