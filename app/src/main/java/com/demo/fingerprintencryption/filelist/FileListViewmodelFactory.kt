package com.demo.fingerprintencryption.filelist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.demo.fingerprintencryption.data.repository.EncryptedFileRepository
import javax.inject.Inject

@FileListScope
class FileListViewmodelFactory @Inject constructor(val repository: EncryptedFileRepository) : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FileListViewModel(repository) as T
    }
}