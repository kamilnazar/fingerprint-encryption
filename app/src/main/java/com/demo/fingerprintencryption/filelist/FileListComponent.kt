package com.demo.fingerprintencryption.filelist

import dagger.Subcomponent

@FileListScope
@Subcomponent(modules = [FileListModule::class])
interface FileListComponent {
    fun viewModel(): FileListViewModel

    fun inject(fragment: FileListFragment)
}