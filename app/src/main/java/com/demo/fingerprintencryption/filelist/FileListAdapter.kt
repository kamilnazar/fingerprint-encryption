package com.demo.fingerprintencryption.filelist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.demo.fingerprintencryption.R
import com.demo.fingerprintencryption.data.entities.EncryptedFileTable
import kotlinx.android.synthetic.main.activity_file_list_item.view.*

class FileListItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(encryptedFile: EncryptedFileTable) {
        view.file_name_list_item.text = encryptedFile.filename
        view.setOnClickListener {
            it.findNavController().navigate(FileListFragmentDirections.actionFileListFragmentToFileDetailFragment(encryptedFile.id))
        }
        if (encryptedFile.completed) {
            view.file_upload_progress.visibility = View.GONE
            view.file_state_icon_list_item.visibility = View.VISIBLE
        } else {
            view.file_upload_progress.visibility = View.VISIBLE
            view.file_state_icon_list_item.visibility = View.GONE
        }
    }
}

class FileListAdapter : RecyclerView.Adapter<FileListItemViewHolder>() {
    private var encryptedFile: List<EncryptedFileTable> = emptyList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FileListItemViewHolder {
        return FileListItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.activity_file_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return encryptedFile.size
    }

    override fun onBindViewHolder(holder: FileListItemViewHolder, position: Int) {
        holder.bind(encryptedFile[position])
    }

    fun updateList(encryptedFile: List<EncryptedFileTable>) {
        this.encryptedFile = encryptedFile
//        this.encryptedFile = encryptedFile
//        DiffUtil.calculateDiff(FileListDiffUtil(oldList, encryptedFile))
//                .dispatchUpdatesTo(this)
        notifyDataSetChanged()
    }
}

class FileListDiffUtil(private val oldList: List<EncryptedFileTable>,
                       private val newList: List<EncryptedFileTable>) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].id == newList[newItemPosition].id

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]
}