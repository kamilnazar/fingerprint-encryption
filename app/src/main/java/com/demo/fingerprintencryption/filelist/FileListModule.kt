package com.demo.fingerprintencryption.filelist

import androidx.lifecycle.ViewModelProviders
import dagger.Module
import dagger.Provides

@Module
class FileListModule(private val fragment: FileListFragment) {

//    @Provides
//    fun provideFactory(repository: EncryptedFileRepository): FileListViewmodelFactory {
//        return FileListViewmodelFactory(repository)
//    }

    @Provides
    fun provideViewModel(factory: FileListViewmodelFactory): FileListViewModel {
        return ViewModelProviders.of(fragment, factory).get(FileListViewModel::class.java)
    }
}