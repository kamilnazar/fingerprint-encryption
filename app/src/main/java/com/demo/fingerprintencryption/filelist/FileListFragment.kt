package com.demo.fingerprintencryption.filelist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.aditya.filebrowser.Constants
import com.aditya.filebrowser.FileChooser
import com.demo.fingerprintencryption.R
import com.demo.fingerprintencryption.base.BaseFragment
import kotlinx.android.synthetic.main.activity_file_list.*
import timber.log.Timber
import javax.inject.Inject


class FileListFragment : BaseFragment() {
    @Inject
    lateinit var viewModel: FileListViewModel
    lateinit var adapter: FileListAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_file_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = FileListAdapter()
        subscribeToViewModel(adapter)
        file_list_recycler_view.layoutManager = LinearLayoutManager(this.activity)
        file_list_recycler_view.adapter = adapter
        file_add_button.setOnClickListener {
            importFile()
        }
    }

    private fun subscribeToViewModel(adapter: FileListAdapter) {
        viewModel.fileList.observe(this, Observer { fileList ->
            adapter.updateList(fileList ?: emptyList())
            adapter.notifyDataSetChanged()
            Timber.d("List changed: $fileList")
        })
    }

    private fun importFile() {
//        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
//            addCategory(Intent.CATEGORY_OPENABLE)
//            type = "*/*"
//        }
//        startActivityForResult(intent, FILE_IMPORT_REQUEST)

        val intent = Intent(activity, FileChooser::class.java)
        intent.putExtra(Constants.SELECTION_MODE, Constants.SELECTION_MODES.SINGLE_SELECTION.ordinal)
        startActivityForResult(intent, FILE_IMPORT_REQUEST)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == FILE_IMPORT_REQUEST && resultCode == Activity.RESULT_OK) {
            Timber.i(data?.data?.path)

            viewModel.scheduleWork(data?.data!!)
        }
    }

    override fun inject() {
        appComponent.subComponent(FileListModule(this))
                .inject(this)
    }

    companion object {
        const val FILE_IMPORT_REQUEST = 4557
    }
}