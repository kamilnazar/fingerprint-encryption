package com.demo.fingerprintencryption.api.requestparams

import com.google.gson.annotations.SerializedName

data class LoginParams(@SerializedName("username") val userName: String,
                       @SerializedName("password") val password: String)