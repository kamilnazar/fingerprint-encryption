package com.demo.fingerprintencryption.api.response

import com.google.gson.annotations.SerializedName

data class EncryptedFile(
        @SerializedName("name") val name: String,
        @SerializedName("downloadUrl") val url: String,
        @SerializedName("extension") val extension: String
)