package com.demo.fingerprintencryption.api

import com.demo.fingerprintencryption.api.requestparams.LoginParams
import com.demo.fingerprintencryption.api.response.EncryptedFile
import com.demo.fingerprintencryption.api.response.Token
import kotlinx.coroutines.Deferred
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface Api {
    @POST("/api-token-auth/")
    fun login(@Body loginParams: LoginParams): Deferred<Token>

    @GET("/files/")
    fun getFiles(): Deferred<List<EncryptedFile>>

    @Multipart
    @POST("/files/")
    fun uploadFile(@Part("name") fileName: RequestBody,
                   @Part("extension") extension: RequestBody,
                   @Part file: MultipartBody.Part): Call<EncryptedFile>
}